FROM python:3

COPY ./ ./

ENV PYTHONPATH="$PYTHONPATH:/"

RUN pip install confluent_kafka

CMD [ "python", "kafkaclientpyhton/app.py" ]