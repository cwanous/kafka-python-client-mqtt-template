﻿# Kafka Python Client
Implementation of a Kafka-Client in Python ready to use with Docker.


### How to code whit it

The *main* method of the project is on *app.py* and its the starting point the developer's Python project. Its in the *main* that the ***CoreElement*** class is instantiated.
 
 All the development is done inside the ***CoreElement*** class. The developer must implement the project logic and make use of the ***update*** and ***send_record*** methods.
			
The ***update*** method handles all the *Kafka-Records* arriving to the consumer. The developer must implement the record receiving treatment.

    def update(self, record):

The ***send_record*** method is used for the sending *Records* to the *Kafka-Broker*. The developer call it whenever it wishes to send *Records* and passes as parameters the topic (*str*) , the key (*str*) and the value (*bytes*) records.

    def send_record(self, topic, key, value):
 
 All clients kafka configuration properties parameters are declared in the *settings.py* file. They must be passes as environment variables. The topics that the client consumer are also declared in this file.

### How to start it

All infrastructure is up to Docker to handle. The docker image kafka-python-client must be build running the *docker build* command and the *Dockerfile* contained in the project. In case the developer needs extra python classes he should declare it in the *Dockerfile*.

    $ docker build . -t kafka_python

After building the image to start it (and all others kafka services) the developer must run:

    $ docker-compose -f docker-compose.yml up

To stop all containers:

    $ docker-compose -f docker-compose.yml down

