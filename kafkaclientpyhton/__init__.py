# code to share
import sys
import logging
from logging import StreamHandler


# LOGGER
logging.getLogger("kafka").addHandler(StreamHandler(sys.stdout))

logging.basicConfig(
    format='[%(levelname)s] %(asctime)s :: %(name)-12s :: %(thread)d ::  %(process)d :: %(message)s',
    level=logging.INFO
)

