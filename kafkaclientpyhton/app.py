import logging
import signal

from kafkaclientpyhton.coreelement import CoreElement

log = logging.getLogger()


def main():
    log.info('Starting Application')

    core = CoreElement()

    signal.signal(signal.SIGINT, core.close)

    """ 
    value_bytes = bytes('5'.encode('utf-8'))

    core.send_record(topic='temperature',
                     key='1',
                     value=value_bytes)
    """


if __name__ == '__main__':
    main()
