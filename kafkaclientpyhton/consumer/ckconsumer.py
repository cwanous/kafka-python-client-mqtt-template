import threading
import logging
from typing import List

from confluent_kafka import Consumer

import kafkaclientpyhton.settings as settings
from kafkaclientpyhton.observer.observer import Observer
from kafkaclientpyhton.observer.subject import Subject


class CKConsumer(threading.Thread, Subject):
    log = logging.getLogger("consumer")
    _observers: List[Observer] = []

    def __init__(self):
        super().__init__()
        self._stopper = threading.Event()

        consumer_topics = self.get_topics()
        self.consumer_properties = self.get_properties()
        self.ckconsumer = Consumer(self.consumer_properties)
        self.ckconsumer.subscribe(consumer_topics)

    @staticmethod
    def get_properties():
        """ Get consumer properties from the environment variables """
        CKConsumer.log.info("Getting consumer properties")
        try:
            properties = {'bootstrap.servers': settings.BOOTSTRAP_SERVERS,
                          'group.id': settings.GROUP_ID,
                          'auto.offset.reset': settings.AUTO_OFFSET_RESET}
            return properties

        except:
            CKConsumer.log.exception("Error getting Consumer properties")
            raise

    @staticmethod
    def get_topics():
        """ Get consumer topics from the environment variables """
        CKConsumer.log.info("Getting topics")
        try:

            return settings.TOPICS.split(',')

        except:
            CKConsumer.log.exception("Error getting topics - no consumer topic")
            raise

    def attach(self, observer: Observer):
        self._observers.append(observer)

    def detach(self, observer: Observer):
        self._observers.remove(observer)

    def notify(self, message):
        """
        Trigger an update in each subscriber.
        """
        try:
            for observer in self._observers:
                observer.update(message)

        except:
            CKConsumer.log.exception('Error notifying listener - record lost')

    def run(self):

        try:
            while True:
                msg = self.ckconsumer.poll(1.0)

                if self.stopped():
                    self.close_consumer()
                    return
                if msg is None:
                    continue
                if msg.error():
                    CKConsumer.log.error('Consumer error: {}'.format(msg.error()))
                    continue

                CKConsumer.log.info('Received Record')
                self.notify(msg)

        except RuntimeError:
            CKConsumer.log.exception('Runtime Error')
            self.ckconsumer.close()

    def close_consumer(self):
        CKConsumer.log.info('Close Consumer')
        try:
            self.ckconsumer.close()
        except RuntimeError:
            pass

    def stop(self):
        CKConsumer.log.info('Stop Consumer')
        self._stopper.set()

    def stopped(self):
        return self._stopper.is_set()
