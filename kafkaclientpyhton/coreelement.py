import logging
import sys

from confluent_kafka import cimpl

from kafkaclientpyhton.consumer.ckconsumer import CKConsumer
from kafkaclientpyhton.observer.observer import Observer
from kafkaclientpyhton.producer.producer import CKProducer
from kafkaclientpyhton.producer.producertask import CKProducerTask


class CoreElement(Observer):
    log = logging.getLogger('coreElement')

    def __init__(self):
        CoreElement.log.info('Initiating Core Element')

        # Producer
        try:
            self.ckproducer = CKProducer()

        except:
            CoreElement.log.exception('Could not initiate Producer')
            sys.exit(1)

        # Consumer
        try:

            self.ckconsumer = CKConsumer()
            self.ckconsumer.attach(self)
            self.ckconsumer.start()

        except:
            CoreElement.log.exception('Could not initiate Consumer')
            sys.exit(1)

    def update(self, record):
        """
        Process every record received from CK Consumer
            :param cimpl.Message record: record
        """
        CoreElement.log.info('New Record Received- Topic: {} - Key: {} - Value: {}'
                             .format(record.topic(), record.key().decode('utf-8'), record.value())
                             )

    def send_record(self, topic, key, value):
        """
        Sends message to Kafka
            :param str topic: topic name
            :param str key: record key
            :param bytes value: record bytes
        """
        CoreElement.log.info('Send Record')
        try:
            CKProducerTask(self.ckproducer, topic=topic, key=key, value=value).start()

        except TypeError:
            CoreElement.log.exception('Could not create send record due to Type Error')

    def close(self, sig, frame):
        """
        Close properly CKConsumer
        """
        CoreElement.log.info('Closing Core Element')
        self.ckconsumer.stop()
