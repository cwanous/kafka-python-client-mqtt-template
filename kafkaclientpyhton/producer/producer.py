import logging
from confluent_kafka.error import KeySerializationError, ValueSerializationError
import kafkaclientpyhton.settings as settings
from confluent_kafka import Producer


class CKProducer(object):
    log = logging.getLogger("producer")

    def __init__(self):
        CKProducer.log.info("Init CK Producer")
        self._properties = self.get_properties()
        self._producer = Producer(self._properties)

    @staticmethod
    def get_properties():
        """ Get producer properties from the environment variables """
        CKProducer.log.info("Getting producer properties")
        try:
            properties = {'bootstrap.servers': settings.BOOTSTRAP_SERVERS,
                          'enable.idempotence': settings.ENABLE_IDEMPOTENCE,
                          'linger.ms': settings.LINGER_MS,
                          'message.timeout.ms': settings.MESSAGE_TIMEOUT_MS,
                          }
            return properties

        except:
            CKProducer.log.exception("Error getting Producer properties")
            raise

    @staticmethod
    def delivery_report(error, msg):
        """ Called once for each message produced to indicate delivery result.
            Triggered by poll() or flush(). """
        if error is not None:
            CKProducer.log.error('Record delivery failed: {}'.format(error))
        else:
            CKProducer.log.info('Record delivered to {}'.format(msg.topic()))

    def produce(self, topic, key, value):
        """
        Sends message to Kafka
            :param str topic: topic name
            :param str key: record key
            :param bytes value: record bytes
        """
        CKProducer.log.info("Producing Record")
        try:

            if type(value) is bytes:
                if type(key) is str:
                    if type(topic) is str:
                        self._producer.poll(0)
                        self._producer.produce(topic,
                                               key=key,
                                               value=value,  # value.encode('utf-8')
                                               callback=self.delivery_report)
                        self._producer.flush()
                    else:
                        raise TypeError('Topic informed not in str format. Format: {}'.format(type(key)))
                else:
                    raise TypeError('Key informed not in str format. Format: {}'.format(type(key)))
            else:
                raise TypeError('Value informed not in bytes format. Format: {}'.format(type(value)))

        except BufferError:
            CKProducer.log.exception('BufferError: internal producer message queue is full')
            raise

        except KeySerializationError:
            CKProducer.log.exception('KeySerializationError: error during key serialization')
            raise

        except ValueSerializationError:
            CKProducer.log.exception('ValueSerializationError: error during value serialization.')
            raise

        except:
            CKProducer.log.exception('Error producing record')
            raise
