import logging
import threading

from kafkaclientpyhton.producer.producer import CKProducer


class CKProducerTask(threading.Thread):
    log = logging.getLogger('producerTask')

    def __init__(self, ckP, topic, key, value):
        """
        Producer Task
            :param CKProducer ckP: CKProducer instance
            :param str topic: record topic
            :param str key: record key
            :param bytes value: record bytes
        """
        if type(ckP) is CKProducer:
            threading.Thread.__init__(self)
            self.ckproducer = ckP
            self.topic = topic
            self.key = key
            self.value = value
        else:
            raise TypeError('CKProducer was not passed')

    def run(self):
        CKProducerTask.log.info('Running Producer Task')
        try:
            self.ckproducer.produce(self.topic,
                                    key=self.key,
                                    value=self.value)

        except:
            CKProducerTask.log.exception("Error producing Task")
