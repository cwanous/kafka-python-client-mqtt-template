import os

# https://github.com/edenhill/librdkafka/blob/master/CONFIGURATION.md

if 'APP_BOOTSTRAP_SERVERS' in os.environ:
    BOOTSTRAP_SERVERS = os.environ.get('APP_BOOTSTRAP_SERVERS')
else:
    BOOTSTRAP_SERVERS = '127.0.0.1:9092'

if 'APP_GROUP_ID' in os.environ:
    GROUP_ID = os.environ.get('APP_GROUP_ID')
else:
    GROUP_ID = 'default'

if 'APP_TOPICS' in os.environ:
    TOPICS = os.environ.get('APP_TOPICS')
else:
    TOPICS = 'temperature,test'

if 'APP_AUTO_OFFSET_RESET' in os.environ:
    AUTO_OFFSET_RESET = os.environ.get('APP_AUTO_OFFSET_RESET')
else:
    AUTO_OFFSET_RESET = 'earliest'

if 'APP_ENABLE_IDEMPOTENCE' in os.environ:
    ENABLE_IDEMPOTENCE = os.environ.get('APP_ENABLE_IDEMPOTENCE')
else:
    ENABLE_IDEMPOTENCE = 'true'

if 'APP_LINGER_MS' in os.environ:
    LINGER_MS = os.environ.get('APP_LINGER_MS')
else:
    LINGER_MS = '1'

if 'APP_MESSAGE_TIMEOUT_MS' in os.environ:
    MESSAGE_TIMEOUT_MS = os.environ.get('APP_MESSAGE_TIMEOUT_MS')
else:
    MESSAGE_TIMEOUT_MS = '5000'

""" 
if '' in os.environ:
     = os.environ.get('')
else:
     = ''
"""