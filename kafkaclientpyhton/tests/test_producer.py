
from confluent_kafka.cimpl import KafkaError

from kafkaclientpyhton.producer.producer import CKProducer


class TestCKProducer(object):

    def setup_class(self):
        pass

    def teardown_class(self):
        pass

    def test_init(self):
        ck = CKProducer()
        assert type(ck) == CKProducer

    def test_get_properties(self):
        ck = CKProducer()
        properties = ck.get_properties()
        assert {'bootstrap.servers', 'enable.idempotence', 'linger.ms', 'message.timeout.ms'}.issubset(properties.keys())

    def test_produce_default(self):
        ck = CKProducer()

        # missing argument
        try:
            ck.produce('temperature')
        except Exception as e:
            assert type(e) == TypeError

        try:
            ck.produce('temperature', key='key')
        except Exception as e:
            assert type(e) == TypeError

        try:
            ck.produce('temperature', value='value')
        except Exception as e:
            assert type(e) == TypeError

        #  wrong type argument
        try:
            ck.produce('temperature', key='key', value='value')
        except Exception as e:
            assert type(e) == TypeError

        try:
            ck.produce(1, key='key', value=bytes('value'.encode('utf-8')))
        except Exception as e:
            assert type(e) == TypeError

        try:
            ck.produce('temperature', key=1, value=bytes('value'.encode('utf-8')))
        except Exception as e:
            assert type(e) == TypeError

        #  success: since there is no broker, produced messages should time out
        try:
            ck.produce('temperature', key='key', value=bytes('value'.encode('utf-8')))
        except Exception as e:
            assert type(e) == KafkaError._MSG_TIMED_OUT
