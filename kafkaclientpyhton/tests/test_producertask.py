from datetime import time

from confluent_kafka.cimpl import KafkaError

from kafkaclientpyhton.producer.producer import CKProducer
from kafkaclientpyhton.producer.producertask import CKProducerTask


class TestCKProducer(object):

    def setup_class(self):
        self.ckP = CKProducer()
        self.topic = 'topic'
        self.key = 'key'
        self.value = bytes('value'.encode('utf-8'))

    def teardown_class(self):
        pass

    def test_init(self):

        ck = CKProducerTask(ckP=self.ckP, topic=self.topic, key=self.key, value=self.value)
        assert type(ck) == CKProducerTask

        try:
            CKProducerTask(ckP=None, topic=self.topic, key=self.key, value=self.value)
        except Exception as e:
            assert type(e) == TypeError

        try:
            CKProducerTask(ckP=self.ckP, topic=None, key=self.key, value=self.value)
        except Exception as e:
            assert type(e) == TypeError

        try:
            CKProducerTask(ckP=self.ckP, topic=self.topic, key=None, value=self.value)
        except Exception as e:
            assert type(e) == TypeError

        try:
            CKProducerTask(ckP=self.ckP, topic=self.topic, key=self.key, value=None)
        except Exception as e:
            assert type(e) == TypeError

    # Mock
    def test_run(self):
        ck = CKProducerTask(ckP=self.ckP, topic=self.topic, key=self.key, value=self.value)

        try:
            ck.start()
            assert ck.is_alive()
        except Exception as e:
            assert type(e) == KafkaError._MSG_TIMED_OUT
